package response

import (
	"encoding/json"
	"net/http"
)

type Response struct {
	Error   bool        `json:"error"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

func UserResponse(w http.ResponseWriter, error bool, statusCode int, message string, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	var response = Response{}
	if error == true {
		response.Error = error
		response.Code = statusCode
		response.Message = message
	}

	response.Code = statusCode
	response.Message = message
	response.Data = data

	json.NewEncoder(w).Encode(&response)
	return
}
