package main

import (
	"gitlab.com/go-on2/crud-books/cfg/db"
	rtr "gitlab.com/go-on2/crud-books/router"
	"log"
	"net/http"
	"os"
)

func main() {
	//init router
	port := os.Getenv("PORT")
	router := rtr.NewRouter()

	//run database
	db.ConnectDB()

	//create http server
	log.Fatal(http.ListenAndServe(":8001"+port, router))
}
