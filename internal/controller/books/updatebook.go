package books

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"gitlab.com/go-on2/crud-books/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

//Upadate Stok book
func UpdatebookHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	params := mux.Vars(r)
	bookId := params["bookId"]
	var book models.Updatebooks
	defer cancel()

	objId, _ := primitive.ObjectIDFromHex(bookId)

	err := BooksCollection.FindOne(ctx, bson.M{"_id": objId}).Decode(&book)
	//validate the booksID
	if err != nil {
		response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
		return
	}

	//validate the request body
	if err := json.NewDecoder(r.Body).Decode(&book); err != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+err.Error(), "")
		return
	}

	//use the validator library to validate required fields
	if validationErr := validate.Struct(&book); validationErr != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+validationErr.Error(), "")
		return
	}

	update := bson.M{"price": book.Price}
	result, err := BooksCollection.UpdateOne(ctx, bson.M{"_id": objId}, bson.M{"$set": update})
	//get updated user details
	var updatedBook models.Updatebooks
	if result.MatchedCount == 1 {
		err := BooksCollection.FindOne(ctx, bson.M{"_id": objId}).Decode(&updatedBook)

		if err != nil {
			response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
			return
		}
	}
	response.UserResponse(w, false, http.StatusOK, "success", &updatedBook)
}
