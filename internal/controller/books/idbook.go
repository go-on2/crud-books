package books

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"gitlab.com/go-on2/crud-books/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

//Get Book by ID
func BookidHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	params := mux.Vars(r)
	booksId := params["booksId"]
	var books models.Books
	defer cancel()

	objId, _ := primitive.ObjectIDFromHex(booksId)

	err := BooksCollection.FindOne(ctx, bson.M{"_id": objId}).Decode(&books)

	if err != nil {
		response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
		return
	}
	response.UserResponse(w, false, http.StatusOK, "success", &books)
	return
}
