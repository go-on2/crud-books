package books

import (
	"context"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"gitlab.com/go-on2/crud-books/cfg/db"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"gitlab.com/go-on2/crud-books/internal/models"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"time"
)

var BooksCollection *mongo.Collection = db.GetCollection(db.DB, "books")
var validate = validator.New()

//Create Books
func CreatebookHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var books models.Books
	defer cancel()

	//validate the request body
	if err := json.NewDecoder(r.Body).Decode(&books); err != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+err.Error(), "")
		return
	}

	//use the validator library to validate required fields
	if validationErr := validate.Struct(&books); validationErr != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+validationErr.Error(), "")
		return
	}

	newBooks := models.Books{
		StoreId: books.StoreId,
		Title:   books.Title,
		Author:  books.Author,
		Isbn:    books.Isbn,
		Genre:   books.Genre,
		Price:   books.Price,
	}
	result, err := BooksCollection.InsertOne(ctx, newBooks)
	if err != nil {
		response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
		return
	}
	var create = result.InsertedID
	response.UserResponse(w, false, http.StatusOK, "success", &create)
}
