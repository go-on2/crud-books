package books

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

//Delete Book by ID
func DeletebookHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	params := mux.Vars(r)
	bookId := params["bookId"]
	defer cancel()
	objId, _ := primitive.ObjectIDFromHex(bookId)

	result, err := BooksCollection.DeleteOne(ctx, bson.M{"_id": objId})

	if err != nil {
		response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
		return
	}

	if result.DeletedCount < 1 {
		response.UserResponse(w, true, http.StatusNotFound, "Error: ", "book with specified ID not found!")
		return
	}
	response.UserResponse(w, false, http.StatusOK, "success", "book successfully deleted!")
}
