package books

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"gitlab.com/go-on2/crud-books/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

//Get book BY STORE ID
func ListbooksHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	params := mux.Vars(r)
	storeid := params["storeid"]
	var book models.Books
	defer cancel()

	objId, _ := primitive.ObjectIDFromHex(storeid)
	ers := BooksCollection.FindOne(ctx, bson.M{"storeid": objId}).Decode(&book)

	if ers != nil {
		response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+ers.Error(), "")
		return
	}

	var books []models.Storeid
	results, err := BooksCollection.Find(ctx, bson.M{"storeid": objId})
	//reading from the db in an optimal way
	defer results.Close(ctx)
	for results.Next(ctx) {
		var singleBook models.Storeid
		if err = results.Decode(&singleBook); err != nil {
			response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
		}

		books = append(books, singleBook)
	}
	response.UserResponse(w, false, http.StatusOK, "success", &books)

}
