package books

import (
	"context"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"gitlab.com/go-on2/crud-books/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"time"
)

//GET All Book
func ListallbooksHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var books []models.Books
	defer cancel()

	results, err := BooksCollection.Find(ctx, bson.M{})

	if err != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+err.Error(), "")
		return
	}

	//reading from the db in an optimal way
	defer results.Close(ctx)
	for results.Next(ctx) {
		var singleBook models.Books
		if err = results.Decode(&singleBook); err != nil {
			response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
		}

		books = append(books, singleBook)
	}
	response.UserResponse(w, false, http.StatusOK, "success", &books)
}
