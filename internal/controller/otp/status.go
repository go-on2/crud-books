package otp

import (
	"crypto/rand"
	"gitlab.com/go-on2/crud-books/cache"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"io"
	"net/http"
)

//Get OTP

func OtpHandler(w http.ResponseWriter, r *http.Request) {
	var (
		redis = cache.NewRedisCache("localhost:6379", 1, 20)
	)

	otp := EncodeToString(6)

	redis.Set(otp, otp)
	response.UserResponse(w, false, http.StatusOK, "success", otp)
}
func EncodeToString(max int) string {
	b := make([]byte, max)
	n, err := io.ReadAtLeast(rand.Reader, b, max)
	if n != max {
		panic(err)
	}
	for i := 0; i < len(b); i++ {
		b[i] = table[int(b[i])%len(table)]
	}
	return string(b)
}

var table = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}
