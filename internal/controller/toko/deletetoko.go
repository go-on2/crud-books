package toko

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

//Delete Toko
func DeletetokoHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	params := mux.Vars(r)
	storeId := params["storeId"]
	defer cancel()

	objId, _ := primitive.ObjectIDFromHex(storeId)
	result, err := StoreCollection.DeleteOne(ctx, bson.M{"_id": objId})

	if err != nil {
		response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
		return
	}
	if result.DeletedCount < 1 {
		response.UserResponse(w, true, http.StatusNotFound, "Error: ", "Toko with specified ID not found!")
		return
	}

	response.UserResponse(w, false, http.StatusOK, "success", "Toko successfully deleted!")
}
