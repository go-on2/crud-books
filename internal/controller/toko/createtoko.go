package toko

import (
	"context"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"gitlab.com/go-on2/crud-books/cfg/db"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"gitlab.com/go-on2/crud-books/internal/models"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"time"
)

var StoreCollection *mongo.Collection = db.GetCollection(db.DB, "store")
var validate = validator.New()

//Create Toko
func CreatetokoHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var store models.Store
	defer cancel()

	//validate the request body
	if err := json.NewDecoder(r.Body).Decode(&store); err != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+err.Error(), "")
		return
	}

	//use the validator library to validate required fields
	if validationErr := validate.Struct(&store); validationErr != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+validationErr.Error(), "")
		return
	}
	newStore := models.Store{
		Name:    store.Name,
		Address: store.Address,
	}
	result, err := StoreCollection.InsertOne(ctx, newStore)
	if err != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+err.Error(), "")
		return
	}
	var create = result.InsertedID
	response.UserResponse(w, false, http.StatusOK, "success", &create)
}
