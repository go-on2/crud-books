package toko

import (
	"context"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"gitlab.com/go-on2/crud-books/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"time"
)

//Get Alltoko
func ListtokoHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var stores []models.Store
	defer cancel()

	results, err := StoreCollection.Find(ctx, bson.M{})

	if err != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+err.Error(), "")
		return
	}
	//reading from the db in an optimal way
	defer results.Close(ctx)
	for results.Next(ctx) {
		var singleStore models.Store
		if err = results.Decode(&singleStore); err != nil {
			response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
		}
		stores = append(stores, singleStore)
	}
	response.UserResponse(w, false, http.StatusOK, "success", &stores)
}
