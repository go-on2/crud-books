package toko

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"gitlab.com/go-on2/crud-books/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

//Update Toko
func UpdatetokoHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	params := mux.Vars(r)
	storeId := params["storeId"]
	var store models.Store
	defer cancel()

	objId, _ := primitive.ObjectIDFromHex(storeId)
	err := StoreCollection.FindOne(ctx, bson.M{"_id": objId}).Decode(&store)
	//validate storeid
	if err != nil {
		response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
		return
	}
	//validate the request body
	if err := json.NewDecoder(r.Body).Decode(&store); err != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+err.Error(), "")
		return
	}

	//use the validator library to validate required fields
	if validationErr := validate.Struct(&store); validationErr != nil {
		response.UserResponse(w, true, http.StatusBadRequest, "Error: "+validationErr.Error(), "")
		return
	}

	update := bson.M{"name": store.Name, "address": store.Address}
	result, err := StoreCollection.UpdateOne(ctx, bson.M{"_id": objId}, bson.M{"$set": update})

	//get updated user details
	var updatedStore models.Store
	if result.MatchedCount == 1 {
		err := StoreCollection.FindOne(ctx, bson.M{"_id": objId}).Decode(&updatedStore)

		if err != nil {
			response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
			return
		}
	}
	response.UserResponse(w, false, http.StatusOK, "success", &updatedStore)
}
