package toko

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/go-on2/crud-books/cfg/response"
	"gitlab.com/go-on2/crud-books/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

//Get Toko by ID
func TokoidHandler(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	params := mux.Vars(r)
	storeId := params["storeId"]
	var store models.Store
	defer cancel()

	objId, _ := primitive.ObjectIDFromHex(storeId)

	err := StoreCollection.FindOne(ctx, bson.M{"_id": objId}).Decode(&store)
	if err != nil {
		response.UserResponse(w, true, http.StatusInternalServerError, "Error: "+err.Error(), "")
		return
	}
	response.UserResponse(w, false, http.StatusOK, "success", &store)
}
