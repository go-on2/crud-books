package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Store struct {
	Id      primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name    string             `json:"name,omitempty" validate:"required"`
	Address string             `json:"address,omitempty" validate:"required"`
}
