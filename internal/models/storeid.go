package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Storeid struct {
	StoreId primitive.ObjectID `json:"storeId,omitempty"`
	Id      primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Title   string             `json:"title,omitempty" validate:"required"`
	Author  string             `json:"author,omitempty" validate:"required"`
	Isbn    int64              `json:"isbn,omitempty" validate:"required"`
	Genre   string             `json:"genre,omitempty" validate:"required"`
	Price   float64            `json:"price,omitempty" validate:"required"`
}
