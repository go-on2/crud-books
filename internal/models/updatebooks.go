package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Updatebooks struct {
	Id      primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	StoreId primitive.ObjectID `json:"storeId,omitempty"`
	Title   string             `json:"title,omitempty"`
	Author  string             `json:"author,omitempty"`
	Genre   string             `json:"genre,omitempty"`
	Price   float64            `json:"price,omitempty" validate:"required"`
}
