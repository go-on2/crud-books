package router

import (
	"gitlab.com/go-on2/crud-books/cfg/router"
	"gitlab.com/go-on2/crud-books/internal/controller/books"
	"gitlab.com/go-on2/crud-books/internal/controller/otp"
	"gitlab.com/go-on2/crud-books/internal/controller/toko"
)

var Routes = router.RoutePrefix{
	"/api",
	[]router.Route{
		//===================================================================================
		//										BOOKS
		//===================================================================================
		router.Route{
			"List Books BY Store ID",
			"GET",
			"/toko/books/{storeid}",
			books.ListbooksHandler,
			false,
		},
		router.Route{
			"List All Books",
			"GET",
			"/toko/books",
			books.ListallbooksHandler,
			false,
		},
		router.Route{
			"Books ID",
			"GET",
			"/toko/books/id/{booksId}",
			books.BookidHandler,
			false,
		},
		router.Route{
			"Create Book",
			"POST",
			"/toko/books",
			books.CreatebookHandler,
			false,
		},
		router.Route{
			"Delete Book",
			"DELETE",
			"/toko/books/{bookId}",
			books.DeletebookHandler,
			false,
		},
		router.Route{
			"Update Stok Book",
			"PUT",
			"/toko/books/{bookId}",
			books.UpdatebookHandler,
			false,
		},

		//===================================================================================
		//										TOKO
		//===================================================================================

		// List Toko
		router.Route{
			"List Toko",
			"GET",
			"/toko",
			toko.ListtokoHandler,
			false,
		},

		// Get Toko ID
		router.Route{
			"Toko ID",
			"GET",
			"/toko/{storeId}",
			toko.TokoidHandler,
			false,
		},

		// Create Toko
		router.Route{
			"Creat Toko",
			"POST",
			"/toko",
			toko.CreatetokoHandler,
			false,
		},
		// Delete Toko
		router.Route{
			"Delete Toko",
			"DELETE",
			"/toko/{storeId}",
			toko.DeletetokoHandler,
			false,
		},
		// Update Toko
		router.Route{
			"Update Toko",
			"PUT",
			"/toko/{storeId}",
			toko.UpdatetokoHandler,
			false,
		},
		//===================================================================================
		//										STATUS
		//===================================================================================

		//Status
		router.Route{
			"Status Check",
			"GET",
			"/generate-otp",
			otp.OtpHandler,
			false,
		},
	},
}
