package router

import (
	"github.com/gorilla/mux"
	customRouter "gitlab.com/go-on2/crud-books/cfg/router"
	"gitlab.com/go-on2/crud-books/internal/router"
	"net/http"
)

func NewRouter() *mux.Router {

	//init router
	routes := mux.NewRouter()

	//append user routes
	customRouter.AppRoutes = append(customRouter.AppRoutes, router.Routes)

	for _, route := range customRouter.AppRoutes {

		//create subroute
		routePrefix := routes.PathPrefix(route.Prefix).Subrouter()

		//loop through each sub route
		for _, r := range route.SubRoutes {

			var handler http.Handler
			handler = r.HandlerFunc

			//attach sub route
			routePrefix.
				Path(r.Pattern).
				Handler(handler).
				Methods(r.Method).
				Name(r.Name)
		}

	}

	return routes
}
